package com.example.ionut.apitest;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, Handler.Callback {

    public static final String QUERY_URL = "http://192.168.114.92/list/index.php?user=";
    public static final int WHAT_MAKE_TEXT = 1;


    private Button button;
    private EditText editText, file_name, comment, user_id, user, pass;
    private TextView response;
    private ProgressDialog progressDialog;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  get list
        editText = (EditText) findViewById(R.id.editText);
        response = (TextView) findViewById(R.id.response);

        //  insert
        user_id = (EditText) findViewById(R.id.user_id);
        comment = (EditText) findViewById(R.id.comment);
        file_name = (EditText) findViewById(R.id.file_name);

        //  login
        user = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Searching..");
        progressDialog.setCancelable(false);

        handler = new Handler(this);
    }

    private void queryUser(String searchString) {
        String urlString = "";

        try {
            urlString = URLEncoder.encode(searchString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        progressDialog.show();

        client.get(QUERY_URL + urlString,
                new JsonHttpResponseHandler() {
                    public void onSuccess(JSONObject jsonObject) {
                        response.setText(jsonObject.optString("data"));
                        progressDialog.dismiss();
                    }

                    public void onFailure(int statusCode, Throwable throwable, JSONObject error) {
                        response.setText(statusCode + " - " + throwable.getMessage());
                        progressDialog.dismiss();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        queryUser(editText.getText().toString());
    }

    public void postData(View view) {

//        try {
//            URL url = new URL("http://192.168.114.92/insert/index.php");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//            conn.setRequestMethod("GET");
//            conn.addRequestProperty("tet", "123");
//            conn.getOutputStream();
//            conn.getInputStream();
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Runnable postRunnable = new Runnable() {
            @Override
            public void run() {
                String user = user_id.getText().toString();
                String com = comment.getText().toString();
                String file = file_name.getText().toString();

                if (user_id.length() > 0 && comment.length() > 0 && file_name.length() > 0) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://192.168.114.92/insert/index.php");

                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    pairs.add(new BasicNameValuePair("user", user));
                    pairs.add(new BasicNameValuePair("comment", com));
                    pairs.add(new BasicNameValuePair("file", file));
                    try {
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    try {
                        HttpResponse response = client.execute(post);
                        responseToJSON(response, "data");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Message msg = new Message();
                    msg.what = WHAT_MAKE_TEXT;
                    msg.obj = "All field are required";
                    handler.sendMessage(msg);
                }
            }
        };

        Thread postThread = new Thread(postRunnable);
        postThread.start();
    }

    public void logIn(View view) {
        Runnable loginRunnable = new Runnable() {
            @Override
            public void run() {
                String username = user.getText().toString();
                String password = pass.getText().toString();

                if (user.length() > 0 && pass.length() > 0) {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost("http://192.168.114.92/login/index.php");

                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    pairs.add(new BasicNameValuePair("user", username));
                    pairs.add(new BasicNameValuePair("pass", password));
                    try {
                        post.setEntity(new UrlEncodedFormEntity(pairs));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    try {
                        HttpResponse response = client.execute(post);
                        responseToJSON(response, "data");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Message msg = new Message();
                    msg.what = WHAT_MAKE_TEXT;
                    msg.obj = "All field are required";
                    handler.sendMessage(msg);
                }
            }
        };

        Thread loginThread = new Thread(loginRunnable);
        loginThread.start();
    }

    private void responseToJSON(HttpResponse response, String field) {
        StringBuilder builder = new StringBuilder();
        HttpEntity entity = response.getEntity();
        InputStream content;
        try {
            content = entity.getContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String readJSON = builder.toString();
        try {
            JSONObject jsonObject = new JSONObject(readJSON);
            String message = jsonObject.getString(field);

            Message msg = Message.obtain();
            msg.obj = message;

            handler.sendMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case WHAT_MAKE_TEXT:
                String text = (String) message.obj;
                Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
                return true;
        }

        return false;
    }
}
